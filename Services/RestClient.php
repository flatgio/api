<?php
/**
 * Created by PhpStorm.
 * User: martintomczak
 * Date: 24.02.19
 * Time: 13:05
 */

namespace App\Services;

use Carbon\Carbon;
use GuzzleHttp\ClientInterface;

/**
 * Class RestClient
 * @package App\Services
 */
class RestClient
{
    /**#
     * @var ClientInterface
     */
    protected $client;


    protected $token = null;

    protected $url = null;

    protected $username = null;

    protected $password = null;

    /**
     * @var array
     */
    protected $config = [];

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }


    /**
     * @param string $url
     * @return $this
     */
    public function setUrl(string $url)
    {
        $this->url = $url;

        return $this;

    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;

    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword(string $password)
    {
        $this->password = $password;

        return $this;

    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken()
    {

        $token = json_decode($this->client->request('POST',$this->url . '/rest/login', [
                    'form_params' => [
                        'username' => $this->username,
                        'password' => $this->password
                    ]
                ])->getBody()->getContents())->accessToken ?? null;


        return $token;
    }


    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function headers()
    {
        return [
            'headers' => [
                'Authorization' => "Bearer {$this->getToken()}"
            ]
        ];
    }

    /**
     * @param int $page
     * @param float $status
     * @param bool $now
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOrders($page = 1, float $status, bool $now = false)
    {

        if($status ==-1) {
            return json_decode($this->client->request('GET',$this->url . '/rest/orders/?statusFrom=0&statusTo=7&page='.$page.'&itemsPerPage=1&createdAtFrom=' . urlencode(Carbon::today()->toW3cString()) . '&createdAtTo=' . urlencode(Carbon::now()->toW3cString()), $this->headers())
                    ->getBody()->getContents()
                ) ?? [];
        }

        if ($now === true) {

            return json_decode($this->client->request('GET',$this->url . '/rest/orders/?statusFrom='.$status.'&statusTo='.$status.'&page='.$page.'&itemsPerPage=1&createdAtFrom=' . urlencode(Carbon::today()->toW3cString()) . '&createdAtTo=' . urlencode(Carbon::now()->toW3cString()), $this->headers())
                    ->getBody()->getContents()
                ) ?? [];
        }

        return json_decode($this->client->request('GET',$this->url . '/rest/orders/?statusFrom='.$status.'&statusTo='.$status.'&page='.$page.'&itemsPerPage=1&createdAtFrom=' . urlencode(Carbon::now()->subMonth(3)->toW3cString()) . '&createdAtTo=' . urlencode(Carbon::now()->toW3cString()), $this->headers())
            ->getBody()->getContents()
        ) ?? [];
    }

    /**
     * @param int $page
     * @param float $status
     * @param bool $now
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPaymentOrdersWithoutPrepayment()
    {
        $array = [
            'statusFrom' => 3,
            'statusTo'   => 3,
            'page'       => 1,
            'itemsPerPage' => 1,
            'createdAtFrom' => Carbon::today()->toW3cString(),
            'createdAtTo'   => Carbon::now()->subDays(3)->toW3cString(),
            'with[]'        => 'payments',

        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/orders/?'.http_build_query($array),$this->headers())
                ->getBody()->getContents()
            ) ?? [];

    }


    /**
     * @param int $page
     * @param null $from
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllReferrers(int $page = 1, $from = null)
    {
        $array = [
            'page'       => $page,
            'itemsPerPage' => 250
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/orders/referrers?'.http_build_query($array),$this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @param int $page
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAllOrdersByDate(int $page = 1, $from = null, int $weeks = 4)
    {

        $array = [
            'page'       => $page,
            'itemsPerPage' => 250,
            'createdAtTo' => Carbon::now()->toW3cString(),
            'createdAtFrom'   => is_null($from) ? Carbon::today()->subWeeks($weeks)->toW3cString() : $from,
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/orders/?'.http_build_query($array),$this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }


    public function getAllOrdersByCredit()
    {
        $array = [
            'statusFrom' => 11,
            'statusTo'   => 11,
            'page'       => 1,
            'itemsPerPage' => 1,
            'createdAtFrom' => Carbon::today()->toW3cString(),
            'createdAtTo'   => Carbon::now()->subDays(7)->toW3cString(),
            'with[]'        => 'payments',

        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/orders/?'.http_build_query($array),$this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }


    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPaymentMethods()
    {
        return json_decode($this->client->request('GET',$this->url . '/rest/payments/methods',$this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getProperties()
    {
        return json_decode($this->client->request('GET',$this->url . '/rest/orders',$this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemByName(string $name)
    {

        $array = [
            'name' => $name,
            'lang' => 'de',
            'page'       => 1,
            'itemsPerPage' => 10,
            'typeId'        => 3
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/items/?'.http_build_query($array), $this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemById(int $id)
    {
        return json_decode($this->client->request('GET',$this->url . '/rest/items/'. $id, $this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemVariationById(int $id)
    {
        $array = [
            'itemId' => $id
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/items/variations/?' . http_build_query($array), $this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @param int $id
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemSalePriceById(int $id)
    {
        $array = [
            'variationId' => $id,
            'salesPriceId' => 5
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/items/variations/variation_sales_prices?'. http_build_query($array), $this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function check()
    {
        return !is_null($this->getToken());
    }

    /**
     * @param int $page
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getArticles(int $page = 1)
    {
        $array = [
            'page' => $page,
            'itemsPerPage' => 250
        ];

        return json_decode($this->client->request('GET',$this->url . '/rest/items/?' . http_build_query($array), $this->headers())
                ->getBody()->getContents()
            ) ?? [];
    }
}
