<?php

namespace App\Criteria;

use App\Entities\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SearchCriteria.
 *
 * @package namespace App\Criteria;
 */
class SearchCriteria implements CriteriaInterface
{

    /**
     * @var Request $request
     */
    protected $request;

    /**
     * SearchCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $keyword = $this->request->query('keyword');

        $mapping = $model->where('keyword', 'LIKE', "{$keyword}%")->orWhere('ref_keyword', 'LIKE', "{$keyword}%");

        $log = Log::where('keyword', $keyword)->first();

        if($log) {

            Log::find($log->id)->update([
                'count' => $log->count + 1
            ]);

        } else {

            Log::create([
                'keyword' => $keyword,
                'count'   => 1,
            ]);
        }

        return $mapping;

    }

    /**
     * @param $term
     * @return string
     */
    protected function fullTextWildcards($term)
    {
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $term = str_replace($reservedSymbols, '', $term);

        $words = explode(' ', $term);

        foreach($words as $key => $word) {
            if(strlen($word) >= 3) {
                $words[$key] = '+' . $word . '*';
            }
        }

        $searchTerm = implode( ' ', $words);

        return $searchTerm;
    }
}
