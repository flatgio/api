<?php

namespace App\Criteria;

use App\Entities\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class SuggestionCriteria.
 *
 * @package namespace App\Criteria;
 */
class SuggestionCriteria implements CriteriaInterface
{



    /**
     * @var Request $request
     */
    protected $request;

    /**
     * SearchCriteria constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {

        $table = DB::table('logs')->where('is_suggestion', 1)
            ->where('keyword', 'LIKE', "{$this->request->query('keyword')}%")
            ->orderBy('count', 'DESC');

        return $table;
    }
}
