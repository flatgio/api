<?php

namespace App\Criteria;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class OrdersWithoutPrepaymentCriteria.
 *
 * @package namespace App\Criteria;
 */
class OrdersPrepaymentCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->whereHas('properties', function(Builder $builder) {
           $builder->where('type_id', 3)
           ->where('value',9);
        })->where('plenty_created_at','<=', Carbon::today()->subDays(3))
        ->where('status_id', 3);
    }
}
