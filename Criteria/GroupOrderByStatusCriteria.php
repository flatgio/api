<?php

namespace App\Criteria;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GroupOrderByStatusCriteria.
 *
 * @package namespace App\Criteria;
 */
class GroupOrderByStatusCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->select(DB::raw('status_id, status_name, count(DISTINCT plenty_id) as num'))
                ->groupBy('status_id');

    }
}
