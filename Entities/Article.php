<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Traits\PresentableTrait;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Article.
 *
 * @package namespace App\Entities;
 */
class Article extends Model implements Presentable
{
    use TransformableTrait, PresentableTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plenty_id',
        'name1',
        'name2',
        'name3',
        'short_description',
        'meta_description',
        'description',
        'technical_data',
        'url_path',
        'keywords',
        'plenty_created_at',
        'plenty_updated_at',
        'shop_id'
    ];

}
