<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Stat extends Model
{
    protected $fillable = [
        'payment',
        'approved_shipped',
        'shipped_now',
        'warehouse',
        'credit',
        'shop_id',
        'ship_prepare'
    ];


    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }
}
