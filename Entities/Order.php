<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\PresentableTrait;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\Entities;
 */
class Order extends Model implements Presentable
{
    use PresentableTrait, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plenty_id',
        'plenty_referrer_id',
        'type_id',
        'lock_status',
        'location_id',
        'plenty_created_at',
        'plenty_updated_at',
        'status_id',
        'owner_id',
        'status_name',
        'status',
        'shop_id'
    ];

    /**
     * @return HasMany
     */
    public function properties(): HasMany
    {
        return $this->hasMany(Property::class);
    }

    /**
     * @return HasMany
     */
    public function orderitems(): HasMany
    {
        return $this->hasMany(Orderitem::class);
    }

    /**
     * @return BelongsTo
     */
    public function ref(): BelongsTo
    {
        return $this->belongsTo(Referrer::class,'plenty_id','plenty_referrer_id');
    }

    /**
     * @return BelongsTo
     */
    public function shop(): BelongsTo
    {
        return $this->belongsTo(Shop::class);
    }

}
