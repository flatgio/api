<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Prettus\Repository\Contracts\Presentable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\PresentableTrait;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Orderitem.
 *
 * @package namespace App\Entities;
 */
class Orderitem extends Model implements Presentable
{
    use PresentableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'order_id',
        'plenty_order_id',
        'plenty_orderitem_id',
        'type_id',
        'referrer_id',
        'item_variation_id',
        'qty',
        'order_item_name',
        'attribute_values',
        'shipping_profile_id',
        'country_vat_id',
        'position',
        'warehouse_id',
        'plenty_created_at',
        'plenty_updated_at',
        'status',
        'price_netto',
        'price_brutto',
        'vat',
        'shop_id'
    ];


    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class);
    }

}
