<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Cron extends Model
{
    /**
     * @var array
     */
     protected $fillable = [
         'start_at',
         'end_at',
         'service'
     ];
}
