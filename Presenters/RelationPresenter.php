<?php

namespace App\Presenters;

use App\Transformers\RelationTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class RelationPresenter.
 *
 * @package namespace App\Presenters;
 */
class RelationPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new RelationTransformer();
    }
}
