<?php

namespace App\Presenters;

use App\Transformers\OrderStatsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrderPresenter.
 *
 * @package namespace App\Presenters;
 */
class OrderStatsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrderStatsTransformer();
    }
}
