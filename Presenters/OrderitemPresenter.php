<?php

namespace App\Presenters;

use App\Transformers\OrderitemTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class OrderitemPresenter.
 *
 * @package namespace App\Presenters;
 */
class OrderitemPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OrderitemTransformer();
    }
}
