<?php

namespace App\Presenters;

use App\Transformers\ReferrerTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ReferrerPresenter.
 *
 * @package namespace App\Presenters;
 */
class ReferrerPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ReferrerTransformer();
    }
}
