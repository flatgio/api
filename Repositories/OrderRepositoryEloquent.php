<?php

namespace App\Repositories;

use App\Services\RestClient;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderRepository;
use App\Entities\Order;
use App\Validators\OrderValidator;

/**
 * Class OrderRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\OrderPresenter";
    }

    /**
     * @param $page
     * @param null $date
     * @return array|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function import($page, $date = null)
    {

        /** @var RestClient $client */
        $client = app(RestClient::class);
        return $client->getAllOrdersByDate($page, $date);
    }
}
