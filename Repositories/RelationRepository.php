<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RelationRepository.
 *
 * @package namespace App\Repositories;
 */
interface RelationRepository extends RepositoryInterface
{
    //
}
