<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ReferrerRepository;
use App\Entities\Referrer;
use App\Validators\ReferrerValidator;

/**
 * Class ReferrerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ReferrerRepositoryEloquent extends BaseRepository implements ReferrerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Referrer::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\ReferrerPresenter";
    }

}
