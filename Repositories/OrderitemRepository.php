<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderitemRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderitemRepository extends RepositoryInterface
{
    //
}
