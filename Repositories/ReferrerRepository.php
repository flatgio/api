<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReferrerRepository.
 *
 * @package namespace App\Repositories;
 */
interface ReferrerRepository extends RepositoryInterface
{
    //
}
