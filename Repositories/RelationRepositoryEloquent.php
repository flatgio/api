<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RelationRepository;
use App\Entities\Relation;
use App\Validators\RelationValidator;

/**
 * Class RelationRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class RelationRepositoryEloquent extends BaseRepository implements RelationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Relation::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
