<?php

namespace App\Repositories;

use App\Criteria\ActiveCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ItemRepository;
use App\Entities\Item;
use App\Validators\ItemValidator;

/**
 * Class ItemRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ItemRepositoryEloquent extends BaseRepository implements ItemRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Item::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(app(ActiveCriteria::class));
    }

    /**
     * @return string
     */
    public function presenter()
    {
        return "App\\Presenters\\ItemPresenter";
    }


}
