<?php

namespace App\Http\Controllers\Api\V1;

use App\Entities\Referrer;
use App\Entities\Shop;
use App\Services\RestClient;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\ShopCreateRequest;
use App\Http\Requests\ShopUpdateRequest;
use App\Repositories\ShopRepository;
use App\Validators\ShopValidator;
use App\Http\Controllers\Controller;

/**
 * Class ShopsController.
 *
 * @package namespace App\Http\Controllers;
 */
class ShopsController extends Controller
{
    /**
     * @var ShopRepository
     */
    protected $repository;

    /**
     * @var ShopValidator
     */
    protected $validator;

    /**
     * ShopsController constructor.
     *
     * @param ShopRepository $repository
     * @param ShopValidator $validator
     */
    public function __construct(ShopRepository $repository, ShopValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $shops = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json($shops);
        }

        return view('shops.index', compact('shops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ShopCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ShopCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $shop = $this->repository->create($request->all());

            $response = [
                'message' => 'Shop created.',
                'data'    => $shop->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $shop = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $shop,
            ]);
        }

        return view('shops.show', compact('shop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $shop = $this->repository->find($id);

        return view('shops.edit', compact('shop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ShopUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ShopUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $shop = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'Shop updated.',
                'data'    => $shop->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Shop deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Shop deleted.');
    }

    /**
     * @param Request $request
     * @param RestClient $restClient
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function apiCheck(Request $request, RestClient $restClient)
    {
        if($request->wantsJson()) {

            $data = $request->only(['url', 'username', 'password']);

            try {

                $client = $restClient->setUrl($data['url'])
                    ->setUsername($data['username'])
                    ->setPassword($data['password'])
                    ->check();

            } catch (ClientException $e) {

            }

            return response()->json(['success' => $client]);

        }
    }

    /**
     * @param Request $request
     * @param RestClient $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function shopUser(Request $request, RestClient $client)
    {
        if($request->wantsJson()) {

            $data = $request->only(['url', 'username', 'password', 'email', 'dashboard_password' ,'name']);

            $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['dashboard_password'])
            ]);


            $shop = Shop::create([
                'plenty_api_url' => $data['url'],
                'plenty_shop' => $data['name'],
                'plenty_username' => $data['username'],
                'plenty_password' => $data['password'],
                'user_id'         => $user->id
            ]);

            if ($shop && $user) {

                $user->assignRole('dashboard');

                $client->setUrl($shop->plenty_api_url)
                    ->setUsername($shop->plenty_username)
                    ->setPassword($shop->plenty_password);

                DB::table('referrers')
                    ->where('shop_id', $shop->id)
                    ->delete();

                foreach ($client->getAllReferrers() as $ref) {

                    Referrer::create([
                        'plenty_id' => $ref->id,
                        'backend_name' => $ref->backendName,
                        'name' => $ref->name,
                        'shop_id' => $shop->id

                    ]);
                }

                return response()->json([
                    'data' => array_merge((array)$user, (array)$shop)
                ]);
            }

            return response()->json(['success' => false], 401);

        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function active(Request $request, $id)
    {
        if($request->wantsJson()) {

            $data = $request->only(['active']);

            $shop = Shop::find($id)->update([
                'is_active' => $data['active']
            ]);

            return response()->json(['success' => $shop]);

        }
    }
}
