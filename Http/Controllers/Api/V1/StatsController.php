<?php

namespace App\Http\Controllers\Api\V1;

use App\Criteria\GroupByReferererCriteria;
use App\Entities\Order;
use App\Entities\Orderitem;
use App\Entities\Referrer;
use App\Entities\Shop;
use App\Presenters\OrderPresenter;
use App\Presenters\OrderStatsPresenter;
use App\Repositories\OrderRepository;
use App\Http\Controllers\Controller;
use App\Transformers\OrderStatsTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class StatsController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $this->repository->pushCriteria(GroupByReferererCriteria::class);
        $this->repository->skipPresenter(OrderPresenter::class);

        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $refs_all = Referrer::whereHas('orders', function (Builder $builder) {
            $builder->where('status', 1);
        })
            ->where('shop_id', $shop->id)
            ->get();

        $refs = Referrer::whereHas('orders', function (Builder $builder) {
            $builder->where('status', 1);
        })
            ->where('shop_id', $shop->id)
            ->where('is_active', 1)->get();

        $data = [];

        foreach ($refs as $ref) {

            $orders = Orderitem::selectRaw('SUM(IF(orders.status_id=11.10, -1 * price_brutto * qty, price_brutto * qty)) as sum')
                ->selectRaw('DATE_FORMAT(orderitems.plenty_created_at,"%Y-%m-%d") as datum')
                ->where('orderitems.status', 1)
                ->where('orderitems.shop_id', $shop->id)
                ->where('orders.plenty_referrer_id', $ref->plenty_id)
                ->join('orders', 'orderitems.order_id', '=', 'orders.id')
                ->orderBy('datum')
                ->groupBy(DB::raw('DATE_FORMAT(orderitems.plenty_created_at,"%Y-%m-%d")'))
                ->get()
                ->toArray();

            $data[] = [
                'name' => $ref->name,
                'is_active' => $ref->is_active,
                'refs'      => $refs_all,
                'id'        => $ref->id,
                'data' => array_map(function ($v) {
                    return [$v['datum'], $v['sum']];

                }, $orders)
            ];
        }

        return response()->json($data, 200);
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function refs()
    {
        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $refs_all = Referrer::whereHas('orders', function (Builder $builder) use ($shop) {
            $builder->where('status', 1)
                ->where('shop_id', $shop->id);
        })->where('shop_id', $shop->id)->get();

        return response()->json($refs_all, 200);
    }
    /**
     * @param Request $request
     * @param $id
     */
    public function active($id, Request $request)
    {

        Referrer::where('id', $id)->update([
            'is_active' => boolval($request->get('is_active'))
        ]);
    }

    public function sales()
    {

        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $orders = Orderitem::selectRaw('SUM(IF(orders.status_id=11.10, -1 * price_brutto * qty, price_brutto * qty)) as sum')
            ->where('orderitems.status', 1)
            ->where('orderitems.shop_id', $shop->id)
            ->join('orders', 'orderitems.order_id', '=', 'orders.id')
            ->whereRaw('DATE_FORMAT(orderitems.plenty_created_at,"%Y-%m-%d") = CURDATE()')
            ->firstOrFail()
            ->toArray();

        $orders_month = Orderitem::selectRaw('SUM(IF(orders.status_id=11.10, -1 * price_brutto * qty, price_brutto * qty)) as sum')
            ->where('orderitems.status', 1)
            ->where('orderitems.shop_id', $shop->id)
            ->join('orders', 'orderitems.order_id', '=', 'orders.id')
            ->whereRaw('DATE_FORMAT(orderitems.plenty_created_at,"%m") = MONTH(NOW())')
            ->firstOrFail()
            ->toArray();

        $orders_year = Orderitem::selectRaw('SUM(IF(orders.status_id=11.10, -1 * price_brutto * qty, price_brutto * qty)) as sum')
            ->where('orderitems.status', 1)
            ->where('orderitems.shop_id', $shop->id)
            ->join('orders', 'orderitems.order_id', '=', 'orders.id')
            ->whereRaw('DATE_FORMAT(orderitems.plenty_created_at,"%Y") = YEAR(NOW())')
            ->firstOrFail()
            ->toArray();


        return response()->json([
            'today' => (string) $orders['sum'],
            'month' => (string) $orders_month['sum'],
            'year' => (string)  $orders_year['sum'],

            ]
        );
    }

    public function dailySales()
    {

        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $orders = Orderitem::selectRaw('SUM(IF(orders.status_id=11.10, -1 * price_brutto * qty, price_brutto * qty)) as sum')
            ->selectRaw('DATE_FORMAT(orderitems.plenty_created_at,"%Y-%m-%d") as datum')
            ->where('orderitems.status', 1)
            ->where('orderitems.shop_id', $shop->id)
            ->where('orders.status_id', '<',8)
            ->join('orders', 'orderitems.order_id', '=', 'orders.id')
            ->orderBy('datum')
            ->groupBy(DB::raw('DATE_FORMAT(orderitems.plenty_created_at,"%Y-%m-%d")'))
            ->get()
            ->toArray();

        $data[] = [
            'name' => 'Umsatz nach Tagen',
            'data' => array_map(function ($v) {
                return [$v['datum'], $v['sum']];

            }, $orders)
        ];

        return response()->json($data, 200);
    }

}