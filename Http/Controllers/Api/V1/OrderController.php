<?php

namespace App\Http\Controllers\Api\V1;

use App\Entities\Cron;
use App\Entities\Order;
use App\Entities\Shop;
use App\Entities\Stat;
use App\Repositories\OrderRepository;
use App\Services\RestClient;
use Carbon\Carbon;
use Dingo\Api\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


/**
 * Class OrderController
 * @package App\Http\Controllers\Api\V1
 */
class OrderController extends Controller
{

    /**
     * @var OrderRepository
     */
    protected $repository;

    /**
     * OrderController constructor.
     * @param OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param RestClient $restClient
     * @return \Illuminate\Http\JsonResponse|string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function index(Request $request, RestClient $restClient)
    {
        $shop = Shop::find($request->user()->shops[0]->id);

        $restClient->setUrl($shop->plenty_api_url)
            ->setUsername($shop->plenty_username)
            ->setPassword($shop->plenty_password);

        $orders = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereNotIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subDays(7), Carbon::now()])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();

        $orders_prepayment = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(7), Carbon::now()->subDays(7)])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();

        $orders_express = Order::whereHas('orderitems', function(Builder $builder) {
            $builder->whereIn('shipping_profile_id', [1,2])
                    ->where('status', 1);

        })->where('status_id','<', 7)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();

        Carbon::setLocale('de');

        $data = [
            'payment' => (string) $restClient->getOrders(1, 3.0)->totalsCount,
            'shipped' => (string) $restClient->getOrders(1, 5.0)->totalsCount,
            'now_shipped' => (string) $restClient->getOrders(1, 6.0)->totalsCount,
            'warehouse' => (string) $restClient->getOrders(1, 7.0, true)->totalsCount,
            'credit'    => (string) $restClient->getAllOrdersByCredit()->totalsCount,
            'time' => Carbon::parse(Cron::where('service')->max('end_at'))->addHours(2)->format('H:i:s'). ' Uhr',
            'without_prepayment' => (string) count($orders),
            'prepayment' => (string) count($orders_prepayment),
            'express'   => (string) count($orders_express)
        ];

       return response()->json($data, 200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderFromDatabase(Request $request)
    {
        $shop = Shop::find($request->user()->shops[0]->id);


        $orders_prepare = Order::whereRaw('DATE_FORMAT(orders.plenty_created_at,"%Y-%m-%d") = CURDATE()')
            ->where('status_id', 4)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_today = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereRaw('DATE_FORMAT(orders.plenty_created_at,"%Y-%m-%d") = CURDATE()')
            ->where('status_id', '<', 8)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_warehouse = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereRaw('DATE_FORMAT(plenty_created_at, "%Y-%m-%d") = CURDATE()')
            ->where('status_id', 7)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_credit = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 11)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_shipped_now = Order::whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 5)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_approved_shipped = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 6)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_payment = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereNotIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subDays(7), Carbon::now()])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_prepayment = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(7), Carbon::now()->subDays(7)])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_express = Order::whereHas('orderitems', function(Builder $builder) {
            $builder->whereIn('shipping_profile_id', [1,2])
                ->where('status', 1);

        })->where('status_id','<', 7)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();

        $data = [
            'time' => Carbon::parse(Cron::where('service', 'orders')->max('end_at'))->addHours(2)->format('H:i:s'). ' Uhr',
            'cron' => Cron::where('service', 'orders')->max('end_at'),
            'without_prepayment' => (string) count($orders),
            'prepayment' => (string) count($orders_prepayment),
            'express'   => (string) count($orders_express),
            'payment' => (string) count($orders_payment),
            'shipped' => (string) count($orders_approved_shipped),
            'now_shipped' => (string) count($orders_shipped_now),
            'warehouse' => (string) count($orders_warehouse),
            'credit'    => (string) count($orders_credit),
            'today'     => (string) count($orders_today),
            'ship_prepare' => (string) count($orders_prepare),
        ];

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderIds(Request $request)
    {
        $shop = Shop::find($request->user()->shops[0]->id);


        $orders_prepare = Order::whereRaw('DATE_FORMAT(orders.plenty_created_at,"%Y-%m-%d") = CURDATE()')
            ->where('status_id', 4)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_today = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereRaw('DATE_FORMAT(orders.plenty_created_at,"%Y-%m-%d") = CURDATE()')
            ->where('status_id', '<', 8)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_warehouse = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereRaw('DATE_FORMAT(plenty_created_at, "%Y-%m-%d") = CURDATE()')
            ->where('status_id', 7)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_credit = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 11)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_shipped_now = Order::whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 5)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_approved_shipped = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 6)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_payment = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(3), Carbon::now()])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereNotIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subDays(7), Carbon::now()])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_prepayment = Order::whereHas('properties', function(Builder $builder) {
            $builder->where('type_id', 3)
                ->whereIn('value', [6000, 0])
                ->where('status', 1);

        })->whereBetween('plenty_created_at',[Carbon::now()->subMonth(7), Carbon::now()->subDays(7)])
            ->where('status_id', 3)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();


        $orders_express = Order::whereHas('orderitems', function(Builder $builder) {
            $builder->whereIn('shipping_profile_id', [1,2])
                ->where('status', 1);

        })->where('status_id','<', 7)
            ->where('status', 1)
            ->where('shop_id', $shop->id)
            ->get();

        $data = [
            'without_prepayment' => $orders,
            'prepayment' => $orders_prepayment,
            'express' => $orders_express,
            'payment' => $orders_payment,
            'now_shipped' => $orders_shipped_now,
            'shipped' => $orders_approved_shipped,
            'warehouse' => $orders_warehouse,
            'credit' => $orders_credit,
            'today' => $orders_today,
            'ship_prepare' => $orders_prepare
        ];

        return response()->json($data, 200);
    }
}
