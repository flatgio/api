<?php

namespace App\Http\Controllers\Api\V1;

use App\Entities\Shop;
use App\Repositories\ItemRepository;
use App\Services\RestClient;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ItemController extends Controller
{

    protected  $repository;

    public function __construct(ItemRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $search
     * @param RestClient $client
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getItemByName(string $search, RestClient $client, Request $request)
    {

        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $client->setUrl($shop->plenty_api_url)
            ->setUsername($shop->plenty_username)
            ->setPassword($shop->plenty_password);


        return response()->json($client->getItemByName($search));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function items()
    {

        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        return response()->json($this->repository->findByField('shop_id', $shop->id));
    }

    /**
     * @param Request $request
     * @param RestClient $client
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function saveItem(Request $request, RestClient $client)
    {
        $shop = Shop::where('user_id', Auth::user()->id)->firstorFail();

        $client->setUrl($shop->plenty_api_url)
            ->setUsername($shop->plenty_username)
            ->setPassword($shop->plenty_password);

        $data = $client->getItemById($request->get('id'));

        $item = $client->getItemVariationById($request->get('id'));

        $priceId = $item->entries[0];

        $price = $client->getItemSalePriceById($priceId->salesPriceVariationId);

        return response()->json($this->repository->create([
            'name' => $data->texts[0]->name1,
            'url'  => $shop->plenty_api_url . '/'  . $data->texts[0]->urlPath . '/a-' . $request->get('id'),
            'price' => round($price->entries[0]->price/ 1.19,2),
            'artnr' => $item->entries[0]->number,
            'item_id'  => $request->get('id'),
            'shop_id'  => Shop::where('user_id', Auth::user()->id)->firstorFail()->id
        ]));
    }

    public function removeItem(int $id)
    {
        return response()->json($this->repository->delete($id));
    }

}
