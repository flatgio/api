<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;
use App\Entities\Log;

/**
 * Class LogTransformer.
 *
 * @package namespace App\Transformers;
 */
class LogTransformer extends TransformerAbstract
{
    /**
     * Transform the Log entity.
     *
     * @param \App\Entities\Log $model
     *
     * @return array
     */
    public function transform(Log $model)
    {
        return [
            'id'         => (int) $model->id,
            'keyword'    => $model->keyword,
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at,
            'created_at_iso' => Carbon::parse($model->created_at)->format('d.m.Y H:i:s'),
            'count'        => (int) $model->count,
            'is_suggestion' => $model->is_suggestion
        ];
    }
}
