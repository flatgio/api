<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Orderitem;

/**
 * Class OrderitemTransformer.
 *
 * @package namespace App\Transformers;
 */
class OrderitemTransformer extends TransformerAbstract
{
    /**
     * Transform the Orderitem entity.
     *
     * @param \App\Entities\Orderitem $model
     *
     * @return array
     */
    public function transform(Orderitem $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
