<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Property;

/**
 * Class PropertyTransformer.
 *
 * @package namespace App\Transformers;
 */
class PropertyTransformer extends TransformerAbstract
{
    /**
     * Transform the Property entity.
     *
     * @param \App\Entities\Property $model
     *
     * @return array
     */
    public function transform(Property $model)
    {
        return $model->toArray();
    }
}
