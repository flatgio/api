<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Relation;

/**
 * Class RelationTransformer.
 *
 * @package namespace App\Transformers;
 */
class RelationTransformer extends TransformerAbstract
{
    /**
     * Transform the Relation entity.
     *
     * @param \App\Entities\Relation $model
     *
     * @return array
     */
    public function transform(Relation $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
