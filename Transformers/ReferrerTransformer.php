<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Referrer;

/**
 * Class ReferrerTransformer.
 *
 * @package namespace App\Transformers;
 */
class ReferrerTransformer extends TransformerAbstract
{
    /**
     * Transform the Referrer entity.
     *
     * @param \App\Entities\Referrer $model
     *
     * @return array
     */
    public function transform(Referrer $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
