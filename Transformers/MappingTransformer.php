<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Mapping;

/**
 * Class MappingTransformer.
 *
 * @package namespace App\Transformers;
 */
class MappingTransformer extends TransformerAbstract
{
    /**
     * Transform the Mapping entity.
     *
     * @param \App\Entities\Mapping $model
     *
     * @return array
     */
    public function transform(Mapping $model)
    {
        return [
            'id'           => (int) $model->id,
            'keyword'      => (string) $model->keyword,
            'ref_keyword'  => (string) $model->ref_keyword,
            'is_suggestion' => (boolean) $model->is_suggestion,
            'created_at'   => $model->created_at,
            'updated_at'   => $model->updated_at
        ];
    }
}
