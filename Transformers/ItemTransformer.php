<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Item;

/**
 * Class ItemTransformer.
 *
 * @package namespace App\Transformers;
 */
class ItemTransformer extends TransformerAbstract
{
    /**
     * Transform the Item entity.
     *
     * @param \App\Entities\Item $model
     *
     * @return array
     */
    public function transform(Item $model)
    {
        return $model->toArray();
    }
}
