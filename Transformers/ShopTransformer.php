<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Shop;

/**
 * Class ShopTransformer.
 *
 * @package namespace App\Transformers;
 */
class ShopTransformer extends TransformerAbstract
{
    /**
     * Transform the Shop entity.
     *
     * @param \App\Entities\Shop $model
     *
     * @return array
     */
    public function transform(Shop $model)
    {
       return $model->toArray();
    }
}
