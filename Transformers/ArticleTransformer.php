<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Article;

/**
 * Class ArticleTransformer.
 *
 * @package namespace App\Transformers;
 */
class ArticleTransformer extends TransformerAbstract
{
    /**
     * Transform the Article entity.
     *
     * @param \App\Entities\Article $model
     *
     * @return array
     */
    public function transform(Article $model)
    {
        return $model->toArray();
    }
}
